<?php

namespace src;

abstract class CompositeRobot extends Robot
{
    private $robots = [];

    /**
     * @return $this
     */
    public function getComposite()
    {
        return $this;
    }

    /**
     * @return Robot []
     */
    protected function getRobots()
    {
        return $this->robots;
    }

    /**
     * @inheritdoc
     */
    public function addRobot(Robot $robot)
    {
        if (!in_array($robot, $this->robots, true)) {
            $this->robots[] = $robot;
        }
    }

    /**
     * @inheritdoc
     */
    public function removeRobot(Robot $robot)
    {
        $this->robots = array_udiff($this->robots, [$robot], function($a, $b){
            return $a === $b ? 0 : 1;
        });
    }

    /**
     * @inheritdoc
     */
    public function getSpeed()
    {
        $robots = $this->getComposite()->getRobots();
        if (count($robots) <= 0) {
            return 0;
        }
        $result = array_shift($robots)->getSpeed();
        foreach ($robots as $robot) {
            $robotSpeed = $robot->getSpeed();
            $result = $robotSpeed < $result ? $robotSpeed : $result;
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getWeight()
    {
        $sum = 0;
        $robots = $this->getComposite()->getRobots();
        foreach ($robots as $robot) {
            $sum += $robot->getWeight();
        }
        return $sum;
    }

    /**
     * @inheritdoc
     */
    public function getHeight()
    {
        $sum = 0;
        $robots = $this->getComposite()->getRobots();
        foreach ($robots as $robot) {
            $sum += $robot->getHeight();
        }
        return $sum;
    }
}
