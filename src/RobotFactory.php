<?php
/**
 * Created by PhpStorm.
 * User: HYS_User
 * Date: 9/21/2016
 * Time: 17:03
 */

namespace src;


class RobotFactory
{
    /**
     * @var Robot []
     */
    private $types = [];


    /**
     * @param Robot $robot
     */
    public function addType(Robot $robot)
    {
        $className = get_class($robot);
        if (!array_key_exists($className, $this->types)) {
            $this->types[get_class($robot)] = clone $robot;
        }
    }

    /**
     * @param Robot $robot
     */
    public function removeType(Robot $robot)
    {
        $className = get_class($robot);
        if (array_key_exists($className, $this->types)) {
            unset($this->types[$className]);
        }
    }

    /**
     * @param $count
     * @return MyHydra1 []
     */
    public function createMyHydra1($count)
    {
        $arr = [];
        for ($i = 0; $i < $count; $i++) {
            $key = 'src\MyHydra1';
            if (array_key_exists($key, $this->types)) {
                $arr[] = clone $this->types[$key];
            }
        }
        return $arr;
    }

    /**
     * @param $count
     * @return MyHydra2 []
     */
    public function createMyHydra2($count)
    {
        $arr = [];
        for ($i = 0; $i < $count; $i++) {
            $key = 'src\MyHydra2';
            if (array_key_exists($key, $this->types)) {
                $arr[] = clone $this->types[$key];
            }
        }
        return $arr;
    }
}