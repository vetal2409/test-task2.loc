<?php

namespace src;

class MyHydra1 extends Robot
{

    /**
     * @return int
     */
    public function getSpeed()
    {
        return 45;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return 10;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return 5;
    }
}
