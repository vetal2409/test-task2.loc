<?php

namespace src;


class RobotScript
{
    /**
     * @param Robot[] $newRobots
     * @param Robot $existingRobot
     * @return CompositeRobot|UnionRobot
     */
    public static function joinExisting(array $newRobots, Robot $existingRobot)
    {
        if (null === ($comp = $existingRobot->getComposite())) {
            $comp = new UnionRobot();
            $comp->addRobot($existingRobot);
        }
        foreach ($newRobots as $robot) {
            $comp->addRobot($robot);
        }

        return $comp;
    }
}
