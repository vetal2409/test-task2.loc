<?php

namespace src;

abstract class Robot
{
    /**
     * @return CompositeRobot
     */
    public function getComposite()
    {
        return null;
    }

    /**
     * @return int
     */
    abstract public function getSpeed();

    /**
     * @return int
     */
    abstract public function getWeight();

    /**
     * @return int
     */
    abstract public function getHeight();
}
