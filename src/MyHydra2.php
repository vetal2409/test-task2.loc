<?php

namespace src;


class MyHydra2 extends Robot
{

    /**
     * @return int
     */
    public function getSpeed()
    {
        return 76;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return 15;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return 60;
    }
}