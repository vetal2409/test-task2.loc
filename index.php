<?php

use src\RobotScript;
use src\UnionRobot;
use src\MyHydra1;
use src\MyHydra2;
use src\RobotFactory;

spl_autoload_register();

$robotFactory = new RobotFactory();
$robotFactory->addType(new MyHydra1());
$robotFactory->addType(new MyHydra2());

$robots = $robotFactory->createMyHydra1(1);
$robots = array_merge($robots, $robotFactory->createMyHydra2(2));

$unionRobot = new UnionRobot();
RobotScript::joinExisting($robotFactory->createMyHydra1(1), $unionRobot);
RobotScript::joinExisting($robotFactory->createMyHydra2(2), $unionRobot);


$unionRobot2 = new UnionRobot();
$duplicatedRobots = $robotFactory->createMyHydra1(3);
RobotScript::joinExisting($robotFactory->createMyHydra1(3), $unionRobot2);

$superUnitRobot = RobotScript::joinExisting([$unionRobot], new UnionRobot());
RobotScript::joinExisting([$unionRobot2], $superUnitRobot);

//todo do not add duplicated objects in composite!
//todo remove robot from any composite
//RobotScript::joinExisting($duplicatedRobots, $superUnitRobot);


var_dump($superUnitRobot->getSpeed());
var_dump($superUnitRobot->getWeight());
var_dump($superUnitRobot->getHeight());
var_dump($superUnitRobot);
